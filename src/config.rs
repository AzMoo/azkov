extern crate toml;

use std::env;
use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use std::process;

pub fn get_default_config() -> PathBuf {
    let mut config_path = env::home_dir()
        .unwrap_or_else(|| {
            println!("Couldn't get the home directory. You've probably screwed your environment like a simpleton.");
            process::exit(4);
        });

    config_path.push(".azkov/Config.toml");
    return config_path;
}

pub fn get_config_toml(config_path: PathBuf) -> toml::Value {
    let mut config_file = File::open(&config_path)
        .unwrap_or_else(|why| {
            println!("You've probably done something monumentally stupid \
                because we couldn't open {}: {}", config_path.display(), why.description());
            process::exit(1)
        });

    let mut config_str = String::new();
    config_file.read_to_string(&mut config_str)
        .unwrap_or_else(|why| {
            println!("Couldn't read the contents of your Config.toml, dumbass. {}", why.description());
            process::exit(2);
        });

    let mut toml_parser = toml::Parser::new(&config_str);
    return match toml_parser.parse() {
        Some(value) => toml::Value::Table(value),
        None => {
            println!("Your Config.toml is badly formed. Get it right, idiot.");
            process::exit(3);
        }
    };
}
