// extern crate rand;
// extern crate hyper;

extern crate getopts;
extern crate toml;
mod config;
mod redis;

// use rand::Rng;
// use redis::Commands;
// use hyper::Client;

use std::env;
use std::process;
use getopts::Options;
use std::path::PathBuf;

fn main() {
    // let con = redis_connect();
    // let http_client = Client::new();
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();
    opts.optopt("c", "config-file", "path to config file", "PATH");

    let matches = opts.parse(&args[1..])
        .unwrap_or_else(|err| {
            println!("You're clearly new to this. Error parsing your options: {}", err.to_string());
            process::exit(1);
        });

    let c = matches.opt_str("c");

    let config_path: PathBuf = match c {
        Some(x) => PathBuf::from(x),
        None => config::get_default_config()
    };

    let config_toml: toml::Value = config::get_config_toml(config_path);
    let redis_connection = redis::redis_connect(config_toml);

    println!("end");
}
