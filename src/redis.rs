extern crate redis;
extern crate toml;

use std::process;

pub fn redis_connect(config_toml: toml::Value) -> redis::Connection {
    let url = match config_toml.lookup("redis.url") {
        Some(v) => v.as_str().unwrap(),
        None => {
            println!("Invalid configuration, stupid. Redis URL is missing.");
            process::exit(4);
        }
    };

    let client = redis::Client::open(url)
        .unwrap_or_else(|err| {
            println!("Error connecting to redis: {}", err.to_string());
            process::exit(4);
        });
    return match client.get_connection() {
        Ok(v) => v,
        Err(_) => {
            println!("Connected to redis but can't get connection object");
            process::exit(4);
        }
    };
}